package com.devops.training.DevopsTraining.controller;

import com.devops.training.DevopsTraining.models.User;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = DevopsController.ROOT_RESOURCE, produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class DevopsController {
    public static final String ROOT_RESOURCE = "/api/user";

    @GetMapping
    public ResponseEntity<User> getUser() {
        return ResponseEntity.ok(new User(1,"SaidDevopsAhmed","SaidTrainingAhmed"));
    }
}
