package com.devops.training.DevopsTraining.models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    private Integer id;
    private String firstName;
    private String lastName;
}
